# Estimating Pi

This program uses a monte carlo method for approximating pi by generating random points on a theoretical square
that has the largest circle possible drawn within it. The distance from the centre is then calculated. A ratio of
points is calculated that fall within to outside the circle. Pi = 4 * (inside circle / total points). 

# The Math

Say a circle has a radius of r that fits perfectly inside a square which must have side length of 2r. The area of 
the circle is given by pi\*r^2 and the square area is given by 4\*r^2. Hence the ratio of random points is given
by (pi\*r^2) / (4\*r^2), which simplyfys to pi/4. Therefore we can get pi = 4 \* ratio. Which is equal to 
4 * (points located inside circle) / (total points).