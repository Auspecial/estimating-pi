import java.util.Random;

public class ApproximatingPi {

	public static void main(String[] args) {
		int i = 0;
		double cCount = 1;
		double count = 1;
		Random random = new Random();
		int repeats = 100000000;
		while (i < repeats) {
			double x = random.nextDouble();
			double y = random.nextDouble();
			double dist = (x*x) + (y*y);
			if (dist <= 1.0)
				cCount++;
			count++;
			i++;
		}
		double pi = 4 * (cCount/count);
		System.out.println(pi);
	}
}